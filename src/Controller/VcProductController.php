<?php

/**
 * This file is a Part of VcProductAPI  Project .
 * @author   Sameh Fajari <semah.fejjari@gmail.com>
 * @license   MIT https://opensource.org/licenses/MIT
 */

namespace App\Controller;

use App\Service\VcProductApiService;
use OpenApi\Annotations as OA;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 *  API Controller EndPoints to fetch Products data
 */
class VcProductController extends AbstractController
{

    #[Route('/products', name: 'vc_products', methods: ["GET"])]
    /**
     * Returns all products by name order
     *
     *  @OA\Get(
     *     path="/products",
     *     summary="Find All Products",
     *     description="Returns a list of products",
     *     operationId="showProducts",
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *      )
     * )
     * @param VcProductApiService $vcProductApiService
     * @return JsonResponse
     */
    public function showProducts(VcProductApiService $vcProductApiService): JsonResponse
    {
        return new JsonResponse([$vcProductApiService->getProductsData()]);
    }

    #[Route('/product/{id}', name: 'vc_product', methods: ["GET"])]
    /**
     * Returns A Single Product By ID
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="The product is not available"
     *     )
     * @param integer $id
     * @param VcProductApiService $vcProductApiService
     * @return JsonResponse
     */
    public function showProduct(int $id, VcProductApiService $vcProductApiService): JsonResponse
    {

        if (empty($vcProductApiService->getProductById($id))) {
            return new JsonResponse([
                "message" => "The product is not available",
            ], 404);
        }
        return new JsonResponse([$vcProductApiService->getProductById($id)]);
    }

    #[Route('/product', name: 'vc_product_search', methods: ["GET"])]
    /**
     * Returns all Products with name containing in the  search
     *  @OA\Get(
     *     path="/product",
     *     summary="Find Products by name filter",
     *     description="Returns a list of products",
     *     operationId="searchProduct",
     *     @OA\Parameter(
     *         description="search",
     *         in="query",
     *         name="search",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *
     *
     *     @OA\Response(
     *         response="404",
     *         description="The product is not available"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Bad request Please add the search Parmeter"
     *     ),
     * )
     * @param Request $request
     * @param VcProductApiService $vcProductApiService
     * @return JsonResponse
     */
    public function searchProduct(Request $request, VcProductApiService $vcProductApiService)
    {

        $search = $request->query->get('search');
        if (empty($search)) {
            return new JsonResponse([
                "message" => "Bad request Please add the search Parmeter",
            ], 400);
        } else {
            $result = $vcProductApiService->getProductByName($search);
            if (empty($result)) {
                return new JsonResponse(status: JsonResponse::HTTP_NO_CONTENT);
            }
            return new JsonResponse([$vcProductApiService->getProductByName($search)]);
        }
    }
}
