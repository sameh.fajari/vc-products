# Vc Product Docker Development Stack
This is a lightweight stack based on Alpine Linux for running Symfony5 into Docker containers using docker-compose.  
### Prerequisites
* [Docker](https://www.docker.com/)
* [Docker-compose](https://docs.docker.com/compose/install/)

### Container
 - [nginx](https://hub.docker.com/_/nginx) 1.19.+
 - [php](https://hub.docker.com/_/php) 8.0.+
    - [composer](https://getcomposer.org/) 

### Installing

run docker and connect to container:
```
cd docker 
 docker-compose up --build
```
install latest version of [Symfony](http://symfony.com/doc/current/setup.html) via composer:
```
# Install API: 
composer install
### For API Documentation:
call [localhost](http://localhost/) in your browser

