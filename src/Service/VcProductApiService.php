<?php

/**
 * This file is a Part of VcProductAPI  Project.
 * @author   Sameh Fajari <semah.fejjari@gmail.com>
 * @license   MIT https://opensource.org/licenses/MIT
 */

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class VcProductApiService
{

    public function __construct(private HttpClientInterface $client)
    {
    }
    /**
     * Fetch data from Json File
     *
     * @return array
     */
    public function getApidata(): array
    {
        $responseToArray = [];
        try {
            $responseToArray = $this->client->request('GET', 'https://maxdesplanches.github.io/products.json', [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ])->toArray();
        } catch (\Symfony\Component\HttpClient\Exception\TransportException | \Exception | \Throwable $exception) {
            throw ($exception);
        }

        return $responseToArray;
    }

    /**
     *   Return A Product  By id
     *
     * @param integer $id
     * @return array
     */
    public function getProductById(int $id): array
    {

        $result = [];
        $response = $this->getApidata();
        foreach ($response as $key => $product) {
            if ($product['id'] == $id) {
                $result = $response[$key];
                break;
            }
        }
        return $result;
    }
    /**
     * Get sorted Data by name
     *
     * @return array
     */
    public function getProductsData(): array
    {
        $result = [];
        $result = $this->getApidata();
        if (!empty($result)) {
            $result =  $this->sortByName($result);
        }


        return $result;
    }

    /**
     * Return an array of products by Name filter
     *
     * @param string $filter
     * @return array
     */
    public function getProductByName(string $filter): array
    {

        $result = [];
        $response = $this->getApidata();
        foreach ($response as $key => $product) {
            if (str_contains(strtolower($product['name']), strtolower($filter))) {
                $result[] = $response[$key];
            }
        }
        if (!empty($result)) {
            $this->sortByName($result);
        }

        return $result;
    }

    /**
     * Order Products by name ascending
     *
     * @param array $result
     * @return array
     */
    public function sortByName(array $array): array
    {
        usort($array, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        return $array;
    }
}
