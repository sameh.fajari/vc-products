<?php

namespace App\EventListener;

use TypeError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

class VcProductExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $message = $exception->getMessage();

        // Customize your response object to display the exception details
        $response = new JsonResponse();
        $response->setContent($message);

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $code =   $exception->getStatusCode();
        } elseif ($exception instanceof RequestExceptionInterface || $exception instanceof TypeError) {
            $code = 400;
            $message = "Bad Request";
        } elseif ($exception instanceof TransportException) {
            $code = 503;
            $message = "Couldn't reach service may be it is offline";
        } else {
            $code = 500;
        }

        $event->setResponse($response);
        $event->setResponse(new JsonResponse([$message], $code));
    }
}
